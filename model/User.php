<?php

namespace Model;


class User {

    public static function login ($params) {

        $login    = isset($params['login'])    ? $params['login']    : '';
        $password = isset($params['password']) ? $params['password'] : '';

        $error    = [];

        if ($login    == '') {
            $error['login']    = 'Обязательное поле';
        }
        if ($password == '') {
            $error['password'] = 'Обязательное поле';
        }
        if (! $error && ($login != 'admin' || $password != '123')) {
            $error['login']    = 'Неверные логин или пароль';
            $error['password'] = 'Неверные логин или пароль';
        }

        return $error;
    }
}

?>
