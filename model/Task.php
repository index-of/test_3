<?php

namespace Model;


class Task {

    private static $perPage = 3;


    public static function countPages () {

        $data = pdoQuery('SELECT count(*) FROM tasks');

        return ceil($data[0][0] / self::$perPage);
    }

    public static function items ($page, $sort) {

        if     ($sort == 'completed') {
            $sort .= ' DESC';
        }
        elseif (! in_array($sort, ['nick', 'email'])) {
            $sort  = 'id DESC';
        }

        $sql   = [];

        $sql[] = 'SELECT * FROM tasks';
        $sql[] = 'ORDER BY ' . $sort;
        $sql[] = sprintf(
            'LIMIT %s, %s',
            ($page - 1) * self::$perPage,
            self::$perPage
        );

        return pdoQuery(join(' ', $sql));
    }

    public static function get ($id) {

        $data = pdoQuery(
            'SELECT * FROM tasks WHERE id = ' . pdoQuote($id)
        );

        return array_shift($data);
    }

    public static function add ($data) {

        $data  = self::norm ($data);
        $error = self::check($data);

        if ($error) {
            return $error;
        }

        $vals = [];

        foreach ($data as $key => $val) {
            $vals[] = $key . ' = ' . pdoQuote($val);
        }

        pdoExec('INSERT INTO tasks SET ' . join(', ', $vals));
    }

    public static function set ($id, $content, $completed) {

        $data  = self::norm (['content' => $content]);
        $error = self::check($data);

        if (isset ($error['content'])) {
            return $error['content'];
        }

        pdoExec(
              'UPDATE tasks '
            . ' SET content   = ' . pdoQuote($content) . ','
            . '     completed = ' . ($completed ? 1 : 0)
            . ' WHERE id = ' . pdoQuote($id)
        );
    }

    // ---------------------------------------------------------------------- //

    private static function norm ($data) {

        $norm = [];

        foreach (['nick', 'email', 'content'] as $key) {
            $norm[$key] = isset($data[$key]) ? trim($data[$key]) : '';
        }

        return $norm;
    }

    private static function check ($data) {

        $error = [];

        if     ($data['nick'] == '') {
            $error['nick'] = 'Обязательное поле';
        }
        elseif (mb_strlen($data['nick']) > 255) {
            $error['nick'] = 'Более 255 символов';
        }

        if     ($data['email'] == '') {
            $error['email'] = 'Обязательное поле';
        }
        elseif (mb_strlen($data['email']) > 255) {
            $error['email'] = 'Более 255 символов';
        }
        elseif (! preg_match('/^[\w.-]+@[a-z-]+(?:\.[a-z]+)+$/', $data['email'])) {
            $error['email'] = 'Неверный адрес';
        }

        if     ($data['content'] == '') {
            $error['content'] = 'Обязательное поле';
        }
        elseif (mb_strlen($data['content']) > 65535) {
            $error['content'] = 'Более 65535 символов';
        }

        return $error;
    }
}

?>
