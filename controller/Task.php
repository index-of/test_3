<?php

namespace Controller;


class Task {

    public static function index () {

        $page  = isset($_REQUEST['page'])
            ? intval($_REQUEST['page'])
            : 0;
        $sort  = isset($_REQUEST['sort'])
            ? $_REQUEST['sort']
            : '';

        $pages = model('Task')->countPages();

        if     ($page < 1) {
            $page = 1;
        }
        elseif ($page > $pages) {
            $page = $pages;
        }

        view('index', [
            'tasks' => model('Task')->items($page, $sort),
            'page'  => $page,
            'pages' => $pages,
            'sort'  => $sort,
        ]);
    }

    public static function addForm () {
        view('add-task');
    }
    public static function add () {

        $error = model('Task')->add($_REQUEST);

        if ($error) {
            view('add-task', ['error' => $error]);
        }
        else {
            header('Location: /', TRUE, 302);
        }
    }

    public static function get ($id) {

        // только для админа
        if (! isAdmin()) {
            return FALSE;
        }

        $task = model('Task')->get($id);

        if (! $task) {
            return FALSE;
        }

        view('task', ['task' => $task]);
    }

    public static function set ($id) {

        // только для админа
        if (! isAdmin()) {
            return FALSE;
        }

        $task  = model('Task')->get($id);

        if (! $task) {
            return FALSE;
        }

        $error = model('Task')->set(
            $id,
            isset ($_REQUEST['content'])
                ?  $_REQUEST['content']
                :  '',
            isset ($_REQUEST['completed'])
                && $_REQUEST['completed']
        );

        if   ($error) {
            view('task', [
                'task'  => $task,
                'error' => $error,
            ]);
        }
        else {
            header('Location: /', TRUE, 302);
        }
    }
}

?>
