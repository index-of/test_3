<?php

namespace Controller;


class User {

    public static function loginForm () {

        if (isAdmin()) {
            return FALSE;
        }

        view('login');
    }
    public static function login () {

        if (isAdmin()) {
            return FALSE;
        }

        $error = model('User')->login($_REQUEST);

        if   ($error) {
            view('login', ['error' => $error]);
        }
        else {
            $_SESSION['isAdmin'] = TRUE;
            header('Location: /', TRUE, 302);
        }
    }

    public static function logout () {

        if (! isAdmin()) {
            return FALSE;
        }

        unset($_SESSION['isAdmin']);
        header('Location: /', TRUE, 302);
    }
}

?>
