<?php
    if (! isset($data)) {
        exit;
    }
?>
<!doctype html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="/css/bootstrap.min.css">
        <title><?php echo isset($data['title']) ? $data['title'] : 'Test' ?></title>
    </head>
    <body>
        <div id="navi" class="container">
            <ul class="list-group list-group-horizontal">
                <li class="list-group-item"><a href="/">Задачи</a></li>
                <li class="list-group-item"><a href="/add-task">Новая задача</a></li>
                <?php if (! isAdmin()): ?>
                    <li class="list-group-item"><a href="/login">Авторизация</a></li>
                <?php else: ?>
                    <li class="list-group-item"><a href="/logout">Выйти</a></li>
                <?php endif; ?>
            </ul>
        </div>
