<?php

    if (! isset($data)) {
        exit;
    }

    $title = 'Новая задача';

    view('_header', ['title' => $title]);
?>
<div class="container">
    <h1><?php echo $title ?></h1>
    <form method="POST">
        <div class="form-group">
            <label for="nick">Имя пользователя</label>
            <input id="nick" type="text" name="nick" class="form-control<?php echo isset($data['error']['nick']) ? ' is-invalid' : '' ?>" value="<?php echo isset($_REQUEST['nick']) ? htmlspecialchars($_REQUEST['nick']) : '' ?>">
            <?php if (isset($data['error']['nick'])): ?>
                <div class="invalid-feedback"><?php echo $data['error']['nick'] ?></div>
            <?php endif; ?>
        </div>
        <div class="form-group">
            <label for="email">Email</label>
            <input id="email" type="text" name="email" class="form-control<?php echo isset($data['error']['email']) ? ' is-invalid' : '' ?>" value="<?php echo isset($_REQUEST['email']) ? htmlspecialchars($_REQUEST['email']) : '' ?>">
            <?php if (isset($data['error']['email'])): ?>
                <div class="invalid-feedback"><?php echo $data['error']['email'] ?></div>
            <?php endif; ?>
        </div>
        <div class="form-group">
            <label for="content">Текст задачи</label>
            <textarea id="content" name="content" class="form-control<?php echo isset($data['error']['content']) ? ' is-invalid' : '' ?>" rows="3"><?php echo isset($_REQUEST['content']) ? htmlspecialchars($_REQUEST['content']) : '' ?></textarea>
            <?php if (isset($data['error']['content'])): ?>
                <div class="invalid-feedback"><?php echo $data['error']['content'] ?></div>
            <?php endif; ?>
        </div>
        <button type="submit" class="btn btn-primary">Добавить</button>
    </form>
</div>
<?php view('_footer') ?>
