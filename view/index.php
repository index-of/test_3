<?php

    if (! isset($data)) {
        exit;
    }

    $sort  = [
        ''          => 'Добавлению',
        'nick'      => 'Имя пользователя',
        'email'     => 'Email',
        'completed' => 'Статус',
    ];

    $title = 'Задачи';

    view('_header', ['title' => $title]);
?>
<div class="container">
    <h1><?php echo $title ?></h1>
    <?php if ($data['tasks']): ?>
        <div id="sort">
            Сортировать по
            <?php foreach ($sort as $key => $name): ?>
                | <a href="?sort=<?php echo $key ?>" class="<?php echo $data['sort'] == $key ? 'active' : '' ?>"><?php echo $name ?></a>
            <?php endforeach; ?>
        </div>
        <?php foreach ($data['tasks'] as $task): ?>
            <div class="card<?php echo $task['completed'] ? ' border-success' : '' ?>">
                <div class="card-body">
                    <h5 class="card-title"><?php echo htmlspecialchars($task['nick']) ?></h5>
                    <h6 class="card-subtitle mb-2 text-muted"><a href="mailto:<?php echo $task['email'] ?>"><?php echo $task['email'] ?></a></h6>
                    <p class="card-text"><?php echo nl2br(htmlspecialchars($task['content'])) ?></p>
                    <p class="card-text"><small class="text-muted"><?php echo $task['completed'] ? 'Выполнено' : 'Выполняется' ?></small></p>
                    <?php if (isAdmin()): ?>
                        <a href="/task/<?php echo $task['id'] ?>" class="card-link">Редактировать</a>
                    <?php endif; ?>
                </div>
            </div>
        <?php endforeach; ?>
    <?php else: ?>
        Нет задач
    <?php endif; ?>
    <?php if ($data['pages'] > 1): ?>
        <nav>
            <ul class="pagination">
                <?php if ($data['page'] > 1): ?>
                    <li class="page-item"><a href="<?php echo htmlspecialchars('?page=' . ($data['page'] - 1) . '&sort=' . $data['sort']) ?>" class="page-link">Предыдущая</a></li>
                <?php endif; ?>
                <?php for ($page = 1; $page <= $data['pages']; $page++): ?>
                    <li class="page-item<?php echo $page == $data['page'] ? ' active' : '' ?>"><a href="<?php echo htmlspecialchars('?page=' . $page . '&sort=' . $data['sort']) ?>" class="page-link"><?php echo $page ?></a></li>
                <?php endfor; ?>
                <?php if ($data['page'] < $data['pages']): ?>
                    <li class="page-item"><a href="<?php echo htmlspecialchars('?page=' . ($data['page'] + 1) . '&sort=' . $data['sort']) ?>" class="page-link">Следующая</a></li>
                <?php endif; ?>
            </ul>
        </nav>
    <?php endif; ?>
</div>
<?php view('_footer') ?>
