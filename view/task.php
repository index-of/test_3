<?php

    if (! isset($data)) {
        exit;
    }

    $title = 'Задача #' . $data['task']['id'];

    view('_header', ['title' => $title]);
?>
<div class="container">
    <h1><?php echo $title ?></h1>
    <form method="POST">
        <div class="form-group">
            <div>Имя пользователя: <strong><?php echo htmlspecialchars($data['task']['nick']) ?></strong></div>
            <div>Email: <strong><?php echo $data['task']['email'] ?></strong></div>
        </div>
        <div class="form-group">
            <label for="content">Текст задачи</label>
            <textarea id="content" name="content" class="form-control<?php echo isset($data['error']) ? ' is-invalid' : '' ?>" rows="3"><?php echo htmlspecialchars($_SERVER['REQUEST_METHOD'] == 'POST' ? (isset($_REQUEST['content']) ? $_REQUEST['content'] : '') : $data['task']['content']) ?></textarea>
            <?php if (isset($data['error'])): ?>
                <div class="invalid-feedback"><?php echo $data['error'] ?></div>
            <?php endif; ?>
        </div>
        <div class="form-group form-check">
            <input type="checkbox" id="completed" name="completed" class="form-check-input"<?php if ($data['task']['completed']) echo ' checked' ?>>
            <label class="form-check-label" for="completed">Выполнено</label>
        </div>
        <button type="submit" class="btn btn-primary">Сохранить</button>
    </form>
</div>
<?php view('_footer') ?>
