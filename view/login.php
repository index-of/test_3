<?php

    if (! isset($data)) {
        exit;
    }

    $title = 'Авторизация';

    view('_header', ['title' => $title]);
?>
<div class="container">
    <h1><?php echo $title ?></h1>
    <form method="POST">
        <div class="form-group">
            <label for="login">Логин</label>
            <input type="text" id="login" name="login" class="form-control<?php echo isset($data['error']['login']) ? ' is-invalid' : '' ?>" value="<?php echo isset($_REQUEST['login']) ? htmlspecialchars($_REQUEST['login']) : '' ?>">
            <?php if (isset($data['error']['login'])): ?>
                <div class="invalid-feedback"><?php echo $data['error']['login'] ?></div>
            <?php endif; ?>
        </div>
        <div class="form-group">
            <label for="password">Пароль</label>
            <input type="password" id="password" name="password" class="form-control<?php echo isset($data['error']['password']) ? ' is-invalid' : '' ?>">
            <?php if (isset($data['error']['password'])): ?>
                <div class="invalid-feedback"><?php echo $data['error']['password'] ?></div>
            <?php endif; ?>
        </div>
        <button type="submit" class="btn btn-primary">Войти</button>
    </form>
</div>
<?php view('_footer') ?>
