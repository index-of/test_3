<?php

error_reporting(E_ALL);

// --- vars ----------------------------------------------------------------- //

$pdo    = new PDO (
    'mysql:host=localhost;dbname={DBNAME}',
    '{USER}',
    '{PASS}'
);

$routes = [

    'GET:'            => 'Task:index',
    'GET:add-task'    => 'Task:addForm',
    'POST:add-task'   => 'Task:add',
    'GET:task/(\d+)'  => 'Task:get',
    'POST:task/(\d+)' => 'Task:set',

    'GET:login'       => 'User:loginForm',
    'POST:login'      => 'User:login',
    'GET:logout'      => 'User:logout',
];

// --- session -------------------------------------------------------------- //

if (! session_id()) {
    session_start();
}

// --- request -------------------------------------------------------------- //

list($path) = explode('?', $_SERVER['REQUEST_URI']);

foreach ($routes as $rule => $controller) {

    list($method, $re) = explode(':', $rule, 2);

    if (   $method == $_SERVER['REQUEST_METHOD']
        && preg_match("#^/${re}/?$#", $path, $params)
    )
    {
        list($name, $fn) = explode(':', $controller);

        require_once  "controller/${name}.php";

        $controller = "Controller\\${name}";
        $controller = new $controller;

        array_shift($params);

        $result     = call_user_func_array(
            [$controller, $fn], $params
        );

        if ($result !== FALSE) {
            break;
        }
    }
}

// --- helpers -------------------------------------------------------------- //

function isAdmin () {
    return isset($_SESSION['isAdmin'])
        ? $_SESSION['isAdmin']
        : FALSE;
}

function pdoQuery ($sql) {

    global $pdo;

    $rows = [];

    foreach ($pdo->query($sql) as $row) {
        $rows[] = $row;
    }

    return $rows;
}
function pdoExec ($sql) {

    global $pdo;

    return $pdo->exec($sql);
}
function pdoQuote ($val) {

    global $pdo;

    return $pdo->quote($val);
}

function model ($name) {

    require_once "model/${name}.php";

    $model = "Model\\${name}";

    return new $model;
}

function view ($name, $data = []) {
    require "view/${name}.php";
}

?>
